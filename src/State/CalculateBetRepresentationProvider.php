<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;;
use App\Dto\CalculateBetOutput;
use App\Entity\Bets;
use App\Entity\Matchs;
use App\Repository\MatchsRepository;
use App\Repository\TeamsRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\JsonResponse;

final class CalculateBetRepresentationProvider implements ProviderInterface
{
    
    public function __construct(
        #[Autowire(service: 'api_platform.doctrine.orm.state.collection_provider')]
        private ProviderInterface $provider, 
        private MatchsRepository $matchsRepository,
        private TeamsRepository $teamsRepository
        )
    {
        
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []) : object|array|null
    {
       
        $bets = $this->provider->provide($operation, $uriVariables, $context);
       
        $listBets = [];

        foreach ($bets as $bet) {

            $match = $this->matchsRepository->find($bet->getIdMatch());
            
            $teamDom = $this->teamsRepository->find($match->getIdTeamHome());
            $teamExt = $this->teamsRepository->find($match->getIdTeamExt());

            $betOutput = new CalculateBetOutput();
            $betOutput->id = "api/bet/" . $bet->getId();
            $betOutput->betDate = $bet->getDate()->format('Y-m-d H:i');
            $betOutput->bet = $bet->getBet();
            $betOutput->uuid_user = $bet->getUuidUser();
            $betOutput->final_result = $bet->getFinalResult();
            $betOutput->amount = $bet->getAmount();
            $betOutput->id_match = $match->getId();
            $betOutput->name_team_home = $teamDom->getName();
            $betOutput->name_team_ext = $teamExt->getName();
            $betOutput->start_date = $match->getStartDate();
            $betOutput->end_date = $match->getEndDate();

            if($match->getStatus() === 'Terminé') {
                $betOutput->calculatedEarnings = $this->calculateEarnings($bet, $match);
            } else {
                $betOutput->calculatedEarnings = null;
            }
            
            $listBets[] = $betOutput;
        }

        usort($listBets, function($a, $b) {
            $dateA = new \DateTime($a->betDate);
            $dateB = new \DateTime($b->betDate);
            return $dateB <=> $dateA;
        });

        return new JsonResponse($listBets);
    }

    private function calculateEarnings(Bets $bet, Matchs $match) {

        $userbet = $bet->getBet();
        $final_result = $bet->getFinalResult();
        $amount = $bet->getAmount();

        if($userbet === $final_result) {
            switch($final_result) {
                case "Victoire domicile" :
                    $solde = $amount * $match->getQuotationTeamHome();
                    break;
                case "Match nul" : 
                    $solde = $amount * $match->getQuotationDraw();
                    break;
                case "Victoire extérieur" :
                    $solde = $amount * $match->getQuotationTeamExt();
            }
        } else {
            $solde = 0 - $amount;
        }

        return $solde;
    }
}