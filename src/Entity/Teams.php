<?php

namespace App\Entity;

use App\Repository\TeamsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [
        new Post(
            name: 'add_team',
            uriTemplate: '/add_team',
            denormalizationContext: ['groups' => 'team:create'],
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Get(
            name: 'team',
            uriTemplate: '/team/{id}',
        ),
        new GetCollection(
            name: 'get_all_team',
            uriTemplate: '/get_all_team',
        )
    ],
    normalizationContext: ['groups' => ['team:read']]
)]
#[ORM\Entity(repositoryClass: TeamsRepository::class)]
class Teams
{

    #[Groups(['team:create', 'team:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['team:create', 'team:read', 'match:read', 'match:readLight'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 120)]
    private ?string $name = null;

    #[Groups(['team:create', 'team:read', 'match:read', 'match:readLight'])]
    #[Assert\NotBlank]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Countries $id_country = null;

    #[Groups(['team:read', 'match:read'])]
    #[ORM\OneToMany(targetEntity: Players::class, mappedBy: 'id_team')]
    private Collection $players;

    public function __construct()
    {
        $this->players = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Players>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Players $player): static
    {
        if (!$this->players->contains($player)) {
            $this->players->add($player);
            $player->setIdTeam($this);
        }

        return $this;
    }

    public function removePlayer(Players $player): static
    {
        if ($this->players->removeElement($player)) {
            // set the owning side to null (unless already changed)
            if ($player->getIdTeam() === $this) {
                $player->setIdTeam(null);
            }
        }

        return $this;
    }

    public function getIdCountry(): ?Countries
    {
        return $this->id_country;
    }

    public function setIdCountry(?Countries $id_country): static
    {
        $this->id_country = $id_country;

        return $this;
    }
}
