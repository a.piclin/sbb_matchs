<?php

namespace App\Entity;

use App\Repository\PlayersRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Post(
            name: 'add_player',
            uriTemplate: '/add_player',
            denormalizationContext: ['groups' => 'player:create'],
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Get(
            name: 'player',
            uriTemplate: '/player/{id}',
        )
    ],
    normalizationContext: ['groups' => ['player:read']]
)]
#[ORM\Entity(repositoryClass: PlayersRepository::class)]
class Players
{
    #[Groups(['player:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['player:create' , 'player:read', 'team:read', 'match:read'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 80)]
    private ?string $firstname = null;

    #[Groups(['player:create' , 'player:read', 'team:read', 'match:read'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 80)]
    private ?string $lastname = null;

    #[Groups(['player:create' , 'player:read'])]
    #[Assert\NotBlank]
    #[ORM\ManyToOne(inversedBy: 'players')]
    private ?Teams $id_team = null;

    #[Groups(['player:create' , 'player:read', 'team:read', 'match:read'])]
    #[ORM\Column(nullable: true)]
    private ?int $number = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getIdTeam(): ?Teams
    {
        return $this->id_team;
    }

    public function setIdTeam(?Teams $id_team): static
    {
        $this->id_team = $id_team;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): static
    {
        $this->number = $number;

        return $this;
    }
}
