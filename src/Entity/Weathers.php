<?php

namespace App\Entity;

use App\Repository\WeathersRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [
        new GetCollection(
            name: 'get_all_weathers',
            uriTemplate: '/get_all_weathers'
        ),
        new Get(
            name: 'weather',
            uriTemplate: '/weather/{id}'
        )
    ],
    normalizationContext: ['groups' => ['weather:read']]
)]
#[ORM\Entity(repositoryClass: WeathersRepository::class)]
class Weathers
{
    #[Groups(['weather:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['weather:read', 'match:read', 'match:readLight'])]
    #[ORM\Column(length: 60)]
    private ?string $label = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }
}
