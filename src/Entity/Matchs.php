<?php

namespace App\Entity;

use App\Repository\MatchsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Post(
            name: 'add_match',
            uriTemplate: '/add_match',
            denormalizationContext: ['groups' => 'match:create'],
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Get(
            name: 'match',
            uriTemplate: '/match/{id}',
        ),
        new GetCollection(
            name: 'get_all_matchs',
            uriTemplate: '/get_all_matchs',
            order: ['start_date' => 'DESC'],
            paginationItemsPerPage: 10
        ),
        new GetCollection(
            name: 'get_all_matchs_of_day',
            uriTemplate: '/get_all_matchs_of_day',
            controller: 'App\Controller\ListMatchController::get_all_matchs_of_day', 
        ),
        new GetCollection(
            name: 'get_all_matchs_to_become',
            uriTemplate: '/get_all_matchs_to_become',
            controller: 'App\Controller\ListMatchController::get_all_matchs_to_become',
            paginationItemsPerPage: 10
        ),
        new Get(
            name: 'get_bets_of_match',
            uriTemplate: '/get_bets_of_match/{id}',
            normalizationContext: ['groups' => 'match:getBets'],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Get(
            name: 'get_comments_of_match',
            uriTemplate: '/get_comments_of_match/{id}',
            normalizationContext: ['groups' => 'match:getComments'],
        ),
        new Patch(
            name: 'add_score_dom',
            uriTemplate: '/add_score_dom/{id}',
            controller: 'App\Controller\TeamPointsController::add_score_dom',
            denormalizationContext: ['groups' => 'match:none'],
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'add_score_dom' => ['type' => 'int', "example" => 6]
                                ]
                            ]
                        ]
                    ],
                ]
                ],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Patch(
            name: 'add_score_ext',
            uriTemplate: '/add_score_ext/{id}',
            controller: 'App\Controller\TeamPointsController::add_score_ext',
            denormalizationContext: ['groups' => 'match:none'],
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'add_score_ext' => ['type' => 'int', "example" => 6]
                                ]
                            ]
                        ]
                    ],
                ]
                ],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Patch(
            name: 'remove_score_dom',
            uriTemplate: '/remove_score_dom/{id}',
            controller: 'App\Controller\TeamPointsController::remove_score_dom',
            denormalizationContext: ['groups' => 'match:none'],
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'remove_score_dom' => ['type' => 'int', "example" => 6]
                                ]
                            ]
                        ]
                    ],
                ]
                ],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Patch(
            name: 'remove_score_ext',
            uriTemplate: '/remove_score_ext/{id}',
            controller: 'App\Controller\TeamPointsController::remove_score_ext',
            denormalizationContext: ['groups' => 'match:none'],
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'remove_score_ext' => ['type' => 'int', "example" => 6]
                                ]
                            ]
                        ]
                    ],
                ]
                ],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Patch(
            name: 'update_weather',
            uriTemplate: '/update_weather/{id}',
            denormalizationContext: ['groups' => 'match:update_weather'],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new Patch(
            name: 'end_match',
            uriTemplate: '/end_match/{id}',
            controller: 'App\Controller\EndMatchController::endMatch',
            denormalizationContext: ['groups' => 'match:end_match'],
            security: "is_granted('ROLE_COMMENT')"
        ),
    ],
    normalizationContext: ['groups' => ['match:read']]
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: MatchsRepository::class)]
class Matchs
{
    #[Groups(['match:read','match:readLight' ])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['match:create', 'match:read','match:readLight'])]
    #[Assert\NotBlank]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Teams $id_team_home = null;

    #[Groups(['match:create', 'match:read', 'match:readLight'])]
    #[Assert\NotBlank]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Teams $id_team_ext = null;

    #[Groups(['match:create', 'match:read', 'match:readLight'])]
    #[Assert\NotBlank]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: false)]
    private ?\DateTime $start_date = null;

    #[Groups(['match:read', 'match:readLight'])]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: false)]
    private ?\DateTime $end_date = null;

    #[Groups(['match:create', 'match:read'])]
    #[ORM\Column]
    private ?float $quotation_team_home = null;

    #[Groups(['match:create', 'match:read'])]
    #[ORM\Column]
    private ?float $quotation_team_ext = null;

    #[Groups(['match:create', 'match:read'])]
    #[ORM\Column(nullable: true)]
    private ?float $quotation_draw = null;

    #[Groups(['match:read', 'match:readLight' ])]
    #[ORM\Column(options: ['default' => 0])]
    private ?int $score_team_home = 0;

    #[Groups(['match:read', 'match:readLight'])]
    #[ORM\Column(options: ['default' => 0])]
    private ?int $score_team_ext = 0;

    #[Groups(['match:create', 'match:update_weather', 'match:read', 'match:readLight'])]
    #[ORM\ManyToOne]
    private ?Weathers $id_weather = null;

    #[Groups(['match:read', 'match:readLight'])]
    #[Assert\Choice(choices: ["A venir", "En cours", "Terminé"])]
    #[ORM\Column(options: ["default" => 'A venir'])]
    private ?string $status = 'A venir';

    #[Groups(['match:getBets'])]
    #[ORM\OneToMany(targetEntity: Bets::class, mappedBy: 'id_match', orphanRemoval: true)]
    private Collection $bets;

    #[Groups(['match:getComments'])]
    #[ORM\OneToMany(targetEntity: Comments::class, mappedBy: 'id_match', orphanRemoval: true)]
    private Collection $comments;

    public function __construct()
    {
        $this->bets = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTeamHome(): ?Teams
    {
        return $this->id_team_home;
    }

    public function setIdTeamHome(?Teams $id_team_home): static
    {
        $this->id_team_home = $id_team_home;

        return $this;
    }

    public function getIdTeamExt(): ?Teams
    {
        return $this->id_team_ext;
    }

    public function setIdTeamExt(?Teams $id_team_ext): static
    {
        $this->id_team_ext = $id_team_ext;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): static
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(?\DateTimeInterface $end_date): static
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getQuotationTeamHome(): ?float
    {
        return $this->quotation_team_home;
    }

    public function setQuotationTeamHome(float $quotation_team_home): static
    {
        $this->quotation_team_home = $quotation_team_home;

        return $this;
    }

    public function getQuotationTeamExt(): ?float
    {
        return $this->quotation_team_ext;
    }

    public function setQuotationTeamExt(float $quotation_team_ext): static
    {
        $this->quotation_team_ext = $quotation_team_ext;

        return $this;
    }

    public function getQuotationDraw(): ?float
    {
        return $this->quotation_draw;
    }

    public function setQuotationDraw(?float $quotation_draw): static
    {
        $this->quotation_draw = $quotation_draw;

        return $this;
    }

    public function getScoreTeamHome(): ?int
    {
        return $this->score_team_home;
    }

    public function setScoreTeamHome(int $score_team_home): static
    {
        $this->score_team_home = $score_team_home;

        return $this;
    }

    public function getScoreTeamExt(): ?int
    {
        return $this->score_team_ext;
    }

    public function setScoreTeamExt(int $score_team_ext): static
    {
        $this->score_team_ext = $score_team_ext;

        return $this;
    }

    public function getIdWeather(): ?Weathers
    {
        return $this->id_weather;
    }

    public function setIdWeather(?Weathers $id_weather): static
    {
        $this->id_weather = $id_weather;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Bets>
     */
    public function getBets(): Collection
    {
        return $this->bets;
    }

    public function addBet(Bets $bet): static
    {
        if (!$this->bets->contains($bet)) {
            $this->bets->add($bet);
            $bet->setIdMatch($this);
        }

        return $this;
    }

    public function removeBet(Bets $bet): static
    {
        if ($this->bets->removeElement($bet)) {
            // set the owning side to null (unless already changed)
            if ($bet->getIdMatch() === $this) {
                $bet->setIdMatch(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comments>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setIdMatch($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getIdMatch() === $this) {
                $comment->setIdMatch(null);
            }
        }

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist()
    {
        $this->end_date = clone $this->start_date;
        $this->end_date->modify('+1 hour');
    }
}
