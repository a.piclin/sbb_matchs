<?php

namespace App\Entity;

use App\Repository\BetsRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;
use ApiPlatform\Metadata\ApiProperty;
use App\Dto\CalculateBetOutput;
use App\State\CalculateBetRepresentationProvider;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeImmutable;

#[ApiResource(
    operations: [
        new Post(
            name: 'add_bet',
            uriTemplate: '/add_bet',
            denormalizationContext: ['groups' => 'bet:create'],
            security: "is_granted('ROLE_USER')"
        ),
        new Post(
            name: 'add_multiple_bet',
            uriTemplate: '/add_multiple_bet',
            controller: 'App\Controller\MultipleBetController::add_multiple_bet', 
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'multiple_bets' => ['type' => 'array'],
                                    'uuid_user' => ['type' => 'string'],
                                ]
                            ]
                        ]
                    ],
                ]
                ],
                security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)",
        ),
        new Get(
            name: 'bet',
            uriTemplate: '/bet/{id}',
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Get(
            name: 'get_my_bet_of_match',
            uriTemplate: '/get_my_bet_of_match/{uuid_user}/{id_match}',
            output: CalculateBetOutput::class,
            provider: CalculateBetRepresentationProvider::class,
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)",
        ),
        new GetCollection(
            name: 'get_all_bets',
            uriTemplate: '/get_all_bets',
            security: "is_granted('ROLE_ADMIN')"
        ),
        new GetCollection(
            name: 'get_all_my_bets',
            uriTemplate: '/get_all_my_bets/{uuid_user}',
            output: CalculateBetOutput::class,
            provider: CalculateBetRepresentationProvider::class,
            normalizationContext: ['groups' => ['bet:custom']],
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)",
        ),
        new GetCollection(
            name: 'get_all_my_bets_not_finished',
            uriTemplate: '/get_all_my_bets_not_finished/{uuid_user}',
            controller: 'App\Controller\ListBetController::get_all_my_bets_not_finished', 
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)"
        ),
        new GetCollection(
            name: 'get_all_my_matchs',
            uriTemplate: '/get_all_my_matchs/{uuid_user}',
            controller: 'App\Controller\ListMatchController::get_all_my_matchs',
            normalizationContext: ['groups' => ['match:readLight']],
        ),
        new Patch(
            name: 'update_bet',
            uriTemplate: '/update_bet/{id}',
            denormalizationContext: ['groups' => 'bet:update'],
            normalizationContext:['groups' => 'none'],
            controller: 'App\Controller\ModifyBetController::update_bet', 
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)"
        ),
        new Delete(
            name: 'delete_bet',
            uriTemplate: '/delete_bet/{id}',
            denormalizationContext: ['groups' => 'bet:delete'],
            controller: 'App\Controller\ModifyBetController::delete_bet', 
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)",
        )
    ],
    normalizationContext: ['groups' => ['bet:read']]
)]
#[ORM\Entity(repositoryClass: BetsRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Bets
{
    #[Groups(['bet:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[Groups(['bet:create', 'bet:read'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 60)]
    private ?string $uuid_user = null;

    #[Groups(['bet:create', 'bet:read'])]
    #[Assert\NotBlank]
    #[ORM\ManyToOne(inversedBy: 'bets')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Matchs $id_match = null;

    #[Groups(['bet:read'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $date = null;

    #[Groups(['bet:create', 'bet:read', 'bet:update'])]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0.50, message: 'Le montant minimum est 0.50')]
    #[ORM\Column]
    private ?float $amount = 0.50;

    #[Groups(['bet:read', 'bet:create', 'bet:update', 'match:getBets'])]
    #[Assert\NotBlank]
    #[ApiProperty(readable: true)]
    #[Assert\Choice(choices:["Victoire domicile", "Match nul", "Victoire extérieur"])]
    #[ORM\Column(nullable:false)]
    private ?string $bet = null;

    #[Groups(['bet:read'])]
    #[Assert\Choice(choices:["Victoire domicile", "Match nul", "Victoire extérieur"])]
    #[ORM\Column(nullable: true)]
    private ?string $final_result = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuidUser(): ?string
    {
        return $this->uuid_user;
    }

    public function setUuidUser(string $uuid_user): static
    {
        $this->uuid_user = $uuid_user;

        return $this;
    }

    public function getIdMatch(): ?Matchs
    {
        return $this->id_match;
    }

    public function setIdMatch(?Matchs $id_match): static
    {
        $this->id_match = $id_match;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function getBet(): ?string
    {
        return $this->bet;
    }

    public function setBet(string $bet): static
    {
        $this->bet = $bet;

        return $this;
    }

    public function getFinalResult(): ?string
    {
        return $this->final_result ?? "En attente";
    }

    public function setFinalResult(string $final_result): static
    {
        $this->final_result = $final_result;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist() {
        $this->date = new DateTimeImmutable();
    }
}
