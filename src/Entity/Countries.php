<?php

namespace App\Entity;

use App\Repository\CountriesRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [
        new GetCollection(
            name: 'get_all_countries',
            uriTemplate: '/get_all_countries',
            paginationItemsPerPage: 200
        ),
        new Get(
            name: 'country',
            uriTemplate: '/country/{id}'
        )
    ],
    normalizationContext: ['groups' => ['country:read']]
)]
#[ORM\Entity(repositoryClass: CountriesRepository::class)]
class Countries
{
    #[Groups(['country:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['country:read', 'match:read','match:readLight'])]
    #[ORM\Column(length: 60)]
    private ?string $name = null;

    #[Groups(['country:read', 'match:read'])]
    #[ORM\Column(length: 3)]
    private ?string $code = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }
}
