<?php

namespace App\Entity;

use App\Repository\CommentsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Attribute\Groups;
use DateTimeImmutable;

#[ApiResource(
    operations: [
        new Post(
            name: 'add_comment',
            uriTemplate: '/add_comment',
            denormalizationContext: ['groups' => 'comment:create'],
            security: "is_granted('ROLE_COMMENT')"
        ),
        new GetCollection(
            name: 'get_all_comments',
            uriTemplate: '/get_all_comments',
            security: "is_granted('ROLE_ADMIN')"
        ),
        new Delete(
            name: 'delete_comment',
            uriTemplate: '/delete_comment/{id}',
            denormalizationContext: ['groups' => 'comment:delete'],
            security: "is_granted('ROLE_COMMENT')"
        )
    ],
    normalizationContext: ['groups' => ['comment:read']]
)]
#[ORM\Entity(repositoryClass: CommentsRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Comments
{
    #[Groups(['comment:read', 'match:getComments'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['comment:create', 'comment:read'])]
    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Matchs $id_match = null;

    #[Groups(['comment:create', 'comment:read', 'match:getComments', ])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    #[Groups(['comment:read', 'match:getComments'])]
    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $date = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdMatch(): ?Matchs
    {
        return $this->id_match;
    }

    public function setIdMatch(?Matchs $id_match): static
    {
        $this->id_match = $id_match;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): static
    {
        $this->date = $date;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist() {
        $this->date = new DateTimeImmutable();
    }
}
