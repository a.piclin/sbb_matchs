<?php

namespace App\Dto;

use ApiPlatform\Metadata\ApiResource;
use DateTime;

#[ApiResource]
class CalculateBetOutput
{
    public string $id;
    public string $uuid_user;
    public string $betDate;
    public string $bet;
    public string $final_result;
    public float $amount;
    public ?float $calculatedEarnings;
    public string $id_match;
    public string $name_team_home;
    public string $name_team_ext;
    public DateTime $start_date;
    public DateTime $end_date;
}