<?php

namespace App\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Doctrine\Orm\Paginator;
use App\Entity\Bets;
use Symfony\Component\HttpFoundation\JsonResponse;

class BetVoter extends Voter
{
    const AUTHORIZE = 'authorize';

    public function __construct(private TokenStorageInterface $tokenStorageInterface, private JWTTokenManagerInterface $jwtManager, private RequestStack $requestStack)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === self::AUTHORIZE && (
            $subject instanceof Paginator ||  $subject instanceof JsonResponse ||  $subject instanceof Bets
        );
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if ($attribute !== self::AUTHORIZE) {
            return false;
        }

        $token = $this->tokenStorageInterface->getToken();

        if (!$token) {
            return false;
        }

        $decodedJwtToken = $this->jwtManager->decode($token);

        if (!$decodedJwtToken['uuid']) {
            return false;
        }

        if ($subject instanceof Paginator) {
            foreach ($subject as $item) {
                if (!$item instanceof Bets) {
                    return false;
                } else {
                    if ($item->getUuidUser() != $decodedJwtToken['uuid']) {
                        return false;
                    }
                }
            }
        }

        if ($subject instanceof Bets) {
            if ($subject->getUuidUser() != $decodedJwtToken['uuid']) {
                return false;
            }
        }

        if ($subject instanceof JsonResponse) {
            $content = $subject->getContent();
            $data = json_decode($content, true);
            foreach ($data as $item) {
                if ($item['uuid_user'] != $decodedJwtToken['uuid']) {
                    return false;
                }
            }
        }

        return true;
    }
}
