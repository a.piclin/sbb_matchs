<?php

namespace App\Repository;

use App\Entity\Bets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Bets>
 */
class BetsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bets::class);
    }

    //    /**
    //     * @return Bets[] Returns an array of Bets objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Bets
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }

    public function findMyBetsNotFinished($uuid): array
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.final_result is null')
            ->andWhere('b.uuid_user = :uuid')
            ->setParameter('uuid', $uuid)
            ->orderBy('b.date', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
