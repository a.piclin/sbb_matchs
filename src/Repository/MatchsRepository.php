<?php

namespace App\Repository;

use App\Entity\Matchs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

use function Symfony\Component\Clock\now;

/**
 * @extends ServiceEntityRepository<Matchs>
 */
class MatchsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Matchs::class);
    }

    //    /**
    //     * @return Matchs[] Returns an array of Matchs objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Matchs
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }

    public function findByToday(): array
    {

        $today = new \DateTime();
        $startDate = $today->format('Y-m-d 00:00:00');
        $endDate = $today->format('Y-m-d 23:59:59');

        return $this->createQueryBuilder('m')
            ->andWhere('m.start_date BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->orderBy('m.start_date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByNotStart(): array
    {
        $now = new \DateTime('now');

        return $this->createQueryBuilder('m')
            ->andWhere('m.status != :enCours')
            ->andWhere('m.status != :termine')
            ->andWhere('m.start_date > :now')
            ->setParameter('enCours', 'En cours')
            ->setParameter('termine', 'Terminé')
            ->setParameter('now', $now)
            ->orderBy('m.start_date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findMatchWithMyBet($uuid): array
    {

        return $this->createQueryBuilder('m')
            ->join('m.bets', 'b')
            ->select('m')
            ->where('b.uuid_user = :uuid')
            ->setParameter('uuid', $uuid)
            ->orderBy('m.start_date', 'DESC')
            ->getQuery()
            ->getResult();
    }

}
