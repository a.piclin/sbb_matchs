<?php

namespace App\Controller;

use App\Entity\Bets;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\MatchsRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MultipleBetController extends AbstractController
{

    public function __construct(private MatchsRepository $matchsRepository, private EntityManagerInterface $em)
    {
    }

    #[Route('/add_multiple_bet', name: 'app_multiple_bet', methods: ['POST'])]
    public function add_multiple_bet(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        if (!isset($data['uuid_user']) || !isset($data['multiple_bets'])) {
            throw new BadRequestException("Contenu  de la requête introuvable");
        }

        $uuid = $data['uuid_user'];

        foreach ($data['multiple_bets'] as $bet) {

            if (!isset($bet['matchId']) || !isset($bet['amount']) || !isset($bet['choiceBet'])) {
                throw new BadRequestException("Données de pari manquantes ou invalides");
            }

            $match = $this->matchsRepository->find($bet['matchId']);

            if (!$match || $this->didStart($match->getStartDate())) {
                throw new BadRequestException("Selection de pari invalide");
            }

            $newBet = new Bets();
            $newBet->setUuidUser($uuid)
                ->setAmount($bet['amount'])
                ->setIdMatch($match)
                ->setBet($bet['choiceBet']);


            $this->em->persist($newBet);
        }

        $this->em->flush();

        return new JsonResponse("Paris placé avec succès", JsonResponse::HTTP_OK);
    }

    private function didStart($date)
    {
        $now = new DateTime('now');
        return $now > $date;
    }
}
