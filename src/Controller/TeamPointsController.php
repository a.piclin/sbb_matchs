<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\MatchsRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class TeamPointsController extends AbstractController
{

    public function __construct(private MatchsRepository $matchsRepository, private EntityManagerinterface $em)
    {
    }

    #[Route('/add_score_dom/{id}', name: 'app_add_score_dom', methods: ['PATCH'])]
    public function add_score_dom($id, Request $request)
    {
        $this->updateScore($id, $request, 'add_score_dom', 'home');
    }

    #[Route('/remove_score_dom/{id}', name: 'app_remove_score_dom', methods: ['PATCH'])]
    public function remove_score_dom($id, Request $request)
    {
        $this->updateScore($id, $request, 'remove_score_dom', 'home', 'remove');
    }

    #[Route('/add_score_ext/{id}', name: 'app_add_score_ext', methods: ['PATCH'])]
    public function add_score_ext($id, Request $request)
    {
        $this->updateScore($id, $request, 'add_score_ext', 'ext');
    }

    #[Route('/remove_score_ext/{id}', name: 'app_remove_score_ext', methods: ['PATCH'])]
    public function remove_score_ext($id, Request $request)
    {
        $this->updateScore($id, $request, 'remove_score_ext', 'ext', 'remove');
    }

    private function updateScore($id, Request $request, $field, $teamType, $action = 'add')
    {
        $match = $this->matchsRepository->find($id);

        if (!$match) {
            throw new BadRequestException("Impossible de mettre à jour ce match.");
        }

        if ($match->getStatus() === 'Terminé') {
            throw new BadRequestException("Vous ne pouvez pas modifier le score d'un match une fois celui-ci terminé.");
        }

        $data = json_decode($request->getContent(), true);

        if (!isset($data[$field])) {
            throw new BadRequestException("Données invalides.");
        }

        $scoreMethod = $teamType === 'home' ? 'getScoreTeamHome' : 'getScoreTeamExt';
        $setScoreMethod = $teamType === 'home' ? 'setScoreTeamHome' : 'setScoreTeamExt';

        $actualScore = $match->$scoreMethod();
        $newScore = $action === 'add' ? $actualScore + $data[$field] : $actualScore - $data[$field];

        if ($newScore < 0) {
            throw new BadRequestException("Impossible de mettre un score négatif.");
        }

        $match->$setScoreMethod($newScore);
        $match->setStatus("En cours");
        $this->em->flush();
    }
 
}
