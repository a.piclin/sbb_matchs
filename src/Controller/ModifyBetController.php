<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\MatchsRepository;
use App\Repository\BetsRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ModifyBetController extends AbstractController
{

    public function __construct(private MatchsRepository $matchsRepository, private BetsRepository $betsRepository, private EntityManagerInterface $em)
    {
    }

    #[Route('/update_bet/{id}', name: 'app_update_bet', methods:['PATCH'])]
    public function update_bet($id, Request $request)
    {
        $bet = $this->betsRepository->find($id);

        if (!$bet) {
            throw new BadRequestException("Impossible de mettre a jour ce pari.");
        }

        $id_match = $bet->getIdMatch();

        $match = $this->matchsRepository->find($id_match);

        if (!$match) {
            throw new BadRequestException("Impossible de mettre a jour ce pari.");
        }

        if($this->didStart($match->getStartDate())) {
            throw new BadRequestException("Impossible de mettre a jour ce pari car le match a déjà commencé ou est le match est terminé.");
        } 
        
        $data = json_decode($request->getContent(), true);

        if(isset($data['amount']) && isset($data['bet'])) {
            $bet->setAmount($data['amount']);
            $bet->setBet($data['bet']);
        }

        $this->em->persist($bet);
        $this->em->flush();

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    #[Route('/delete_bet/{id}', name: 'app_delete_bet', methods:['DELETE'])]
    public function delete_bet($id)
    {
        $bet = $this->betsRepository->find($id);

        if (!$bet) {
            throw new BadRequestException("Impossible de supprimer ce pari.");
        }

        $id_match = $bet->getIdMatch();
        $match = $this->matchsRepository->find($id_match);

        if (!$match) {
            throw new BadRequestException("Impossible de supprimer ce pari.");
        }

        if ($this->didStart($match->getStartDate())) {
            throw new BadRequestException("Impossible de supprimer ce pari car le match a déjà commencé.");
        }

        $this->em->remove($bet);
        $this->em->flush();

        return new JsonResponse(null, JsonResponse::HTTP_OK);
    }


    private function didStart($date) {
        $now = new DateTime('now');
        return $now > $date;
    }
}
