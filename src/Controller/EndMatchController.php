<?php

namespace App\Controller;

use App\Entity\Matchs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\MatchsRepository;
use DateTime;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;

class EndMatchController extends AbstractController
{

    public function __construct(private MatchsRepository $matchsRepository, private EntityManagerinterface $em)
    {
    }


    #[Route('/end_match/{id}', name: 'app_end_match', methods: ['PATCH'])]
    public function endMatch($id)
    {
        $match = $this->matchsRepository->find($id);

        if (!$match) {
            throw new BadRequestException("Impossible de mettre a jour ce match.");
        }

        $this->makeUpdateMatchandBets($match);
    }

    private function calculateResultOfMatch(Matchs $match)
    {
        if ($match->getScoreTeamHome() > $match->getScoreTeamExt()) {
            $result = "Victoire domicile";
        } else if ($match->getScoreTeamHome() < $match->getScoreTeamExt()) {
            $result = "Victoire extérieur";
        } else {
            $result = "Match nul";
        }

        return $result;
    }

    private function makeUpdateMatchandBets(Matchs $match)
    {

        $result = $this->calculateResultOfMatch($match);

        $actualDate = new DateTime('now');

        $match->setStatus("Terminé");

        if($match->getEndDate() < $actualDate ) {
            $match->setEndDate($actualDate);
        }

        $bets = $match->getBets();

        foreach ($bets as $bet) {
            $bet->setFinalResult($result);
        }

        $this->em->flush();
    }
}
