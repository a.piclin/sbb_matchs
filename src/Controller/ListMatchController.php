<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\MatchsRepository;

class ListMatchController extends AbstractController
{

    public function __construct(private MatchsRepository $matchsRepository)
    {
    }


    #[Route('/get_all_matchs_of_day', name: 'app_all_matchs_of_day', methods:['GET'])]
    public function get_all_matchs_of_day()
    {
        return $this->matchsRepository->findByToday();
    }

    #[Route('/get_all_matchs_to_become', name: 'app_all_matchs_to_become', methods:['GET'])]
    public function get_all_matchs_to_become()
    {
        return $this->matchsRepository->findByNotStart();
    }

    
    #[Route('/get_all_my_matchs/{uuid_user}', name: 'app_all_my_matchs', methods:['GET'])]
    public function get_all_my_matchs($uuid_user)
    {

        return $this->matchsRepository->findMatchWithMyBet($uuid_user);
    }


}
