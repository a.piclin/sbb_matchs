<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\BetsRepository;

class ListBetController extends AbstractController
{

    public function __construct(private BetsRepository $betsRepository)
    {
    }


    #[Route('/get_all_my_bets_not_finished/{uuid_user}', name: 'app_all_my_bets_not_finished', methods:['GET'])]
    public function get_all_my_bets_not_finished($uuid_user)
    {
        return $this->betsRepository->findMyBetsNotFinished($uuid_user);
    }
}
