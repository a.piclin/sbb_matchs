<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240614115236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE countries_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE players_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE teams_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE countries (id INT NOT NULL, name VARCHAR(60) NOT NULL, code VARCHAR(3) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE players (id INT NOT NULL, id_team_id INT DEFAULT NULL, firstname VARCHAR(80) NOT NULL, lastname VARCHAR(80) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_264E43A6F7F171DE ON players (id_team_id)');
        $this->addSql('CREATE TABLE teams (id INT NOT NULL, id_country_id INT NOT NULL, name VARCHAR(120) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_96C222585CA5BEA7 ON teams (id_country_id)');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A6F7F171DE FOREIGN KEY (id_team_id) REFERENCES teams (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE teams ADD CONSTRAINT FK_96C222585CA5BEA7 FOREIGN KEY (id_country_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE countries_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE players_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE teams_id_seq CASCADE');
        $this->addSql('ALTER TABLE players DROP CONSTRAINT FK_264E43A6F7F171DE');
        $this->addSql('ALTER TABLE teams DROP CONSTRAINT FK_96C222585CA5BEA7');
        $this->addSql('DROP TABLE countries');
        $this->addSql('DROP TABLE players');
        $this->addSql('DROP TABLE teams');
    }
}
