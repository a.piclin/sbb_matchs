<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240614152032 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE bets_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bets (id INT NOT NULL, id_match_id INT NOT NULL, uuid_user VARCHAR(60) NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, amount DOUBLE PRECISION NOT NULL, bet VARCHAR(255) NOT NULL, final_result VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7C28752B7A654043 ON bets (id_match_id)');
        $this->addSql('COMMENT ON COLUMN bets.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE bets ADD CONSTRAINT FK_7C28752B7A654043 FOREIGN KEY (id_match_id) REFERENCES matchs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE bets_id_seq CASCADE');
        $this->addSql('ALTER TABLE bets DROP CONSTRAINT FK_7C28752B7A654043');
        $this->addSql('DROP TABLE bets');
    }
}
