<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240617095840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matchs ALTER score_team_home SET DEFAULT 0');
        $this->addSql('ALTER TABLE matchs ALTER score_team_ext SET DEFAULT 0');
        $this->addSql('ALTER TABLE matchs ALTER status SET DEFAULT \'A venir\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE matchs ALTER score_team_home DROP DEFAULT');
        $this->addSql('ALTER TABLE matchs ALTER score_team_ext DROP DEFAULT');
        $this->addSql('ALTER TABLE matchs ALTER status DROP DEFAULT');
    }
}
