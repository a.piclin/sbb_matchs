<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240614141055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE matchs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE weathers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE matchs (id INT NOT NULL, id_team_home_id INT NOT NULL, id_team_ext_id INT NOT NULL, id_weather_id INT DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, quotation_team_home DOUBLE PRECISION NOT NULL, quotation_team_ext DOUBLE PRECISION NOT NULL, quotation_draw DOUBLE PRECISION DEFAULT NULL, score_team_home INT NOT NULL, score_team_ext INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6B1E6041F475CCD9 ON matchs (id_team_home_id)');
        $this->addSql('CREATE INDEX IDX_6B1E6041330D3583 ON matchs (id_team_ext_id)');
        $this->addSql('CREATE INDEX IDX_6B1E6041AD44E789 ON matchs (id_weather_id)');
        $this->addSql('CREATE TABLE weathers (id INT NOT NULL, label VARCHAR(60) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E6041F475CCD9 FOREIGN KEY (id_team_home_id) REFERENCES teams (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E6041330D3583 FOREIGN KEY (id_team_ext_id) REFERENCES teams (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E6041AD44E789 FOREIGN KEY (id_weather_id) REFERENCES weathers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE matchs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE weathers_id_seq CASCADE');
        $this->addSql('ALTER TABLE matchs DROP CONSTRAINT FK_6B1E6041F475CCD9');
        $this->addSql('ALTER TABLE matchs DROP CONSTRAINT FK_6B1E6041330D3583');
        $this->addSql('ALTER TABLE matchs DROP CONSTRAINT FK_6B1E6041AD44E789');
        $this->addSql('DROP TABLE matchs');
        $this->addSql('DROP TABLE weathers');
    }
}
